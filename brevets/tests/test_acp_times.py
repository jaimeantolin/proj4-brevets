

from acp_times import *

import nose    # Testing framework
import logging
import arrow

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)



def test_normal200():

    assert open_time(120,200, "2017-01-01 00:00") == arrow.get("2017-01-01 03:32").isoformat()
    assert close_time(120,200, "2017-01-01 00:00") == arrow.get("2017-01-01 08:00").isoformat()

def test_under60():

	assert open_time(20,600, "2017-01-01 00:00") == arrow.get("2017-01-01 00:35").isoformat()
	assert close_time(20,600, "2017-01-01 00:00") == arrow.get("2017-01-01 02:00").isoformat()

def test_longer_distance():

	assert open_time(422,400, "2017-01-01 00:00") == arrow.get("2017-01-01 12:08").isoformat()
	assert close_time(422,400, "2017-01-01 00:00") == arrow.get("2017-01-02 03:00").isoformat()

def test_error():

	assert open_time(1201,1000, "2017-01-01 00:00") == arrow.get("2016-12-31 23:00").isoformat()
	assert close_time(1201,1000, "2017-01-01 00:00") == arrow.get("2016-12-31 23:00").isoformat()

def test_concatenated_sums():

	assert open_time(890,1000, "2017-01-01 00:00") == arrow.get("2017-01-02 05:09").isoformat()
	assert close_time(890,1000, "2017-01-01 00:00") == arrow.get("2017-01-03 17:23").isoformat()

