# Project 4: Brevet time calculator with Ajax

Reimplementation of the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times calculator

This calculator provides open and close times for given controls based on the total brevet distance and the initial time and date of the event. Controls are points where a rider must obtain proof of passage, and control times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). 

## Users

In order to use this calculator effectively 4 inputs are required from you:
	
* Distance: using the drop down you must select the total distance of your brevet (it is okay if your total distance is a bit longer, just pick the closest one we will discuss this later)
* Start date: The date on when you plan to start your brevet
* Start time: The start time of when you plan to start the brevet
* Location of controls: in each row of the calculator you can either enter a distance in km or miles (translation will be computed automatically) 

Your open and close times will now automatically appear in the cells of the same row. The notes column is to specify error messages.

Rules:

* Final control cannot be further than 20% of the total distance of the brevet indicated. If it is error message will be displayed for that control.
* Controls in the first 60km will be calculated differently and are not encouraged (to see how times are calculated check developer section).

## Developers

In this section I will walk through every file I have created or modified and explain my implementations.

### calc.html:

This is the file that controls the appearance of the website that is displayed on the server. Everything is organized in tables and cells which are appropriately labeled by ids and classes to ease modifying them. In the script part their are functions which use AJAX for calculating and translating from km to miles and vice versa($(document).ready(function()). Then we have the function "calc_times(control)" which is also implemented with AJAX, here we extract all of the data we need from our site and package it with JSON and send it to our server to calculate the times. Once we get these back we print in the correct places the times. Error control is also performed here. Moment.js is used to handle time manipulation and format, it is the javascript equivalent of arrow in python. 

### flask_brevets.py:

This is the server side implementation using flask. Here pages are loaded and modified. The calc_times function is invoked everytime a control distance is inputed on the client side (a JSON request is sent). This method unpacks all of the necessary information that is sent and calls the methods in test_acp_times.py to calculate and return the times. Once this is done it packages the results into a JSON object and returns it to the client side to update the site. Brevet start date must be converted to an arrow object before being passed to the functions.

### acp_times.py:

In this file the open and close time calculations are performed. We have a global variable table that stores the min and max velocities for each distance section. I will first explain the general algorithm for both functions and then discuss the special cases for each.

We have a km_left variable that at the start is equal to total distance of the control, and we have an open/close_time variable. We will modify both of these as we check distances. As we know that calculations for longer distances must be a sum of the times taken to cover specific periods of the control. 

Ex: Consider a control at 890km on a 1000km brevet. 200/34 + 200/32 + 200/30 + 290/28 = 29H09

So what the basic algorithm does is check if the km_left is longer than each min distance for a change in speeds and stack the answer on open/close_time then the km_left is updated by subtracting the chunk of km we have just calculated the time for. At the end it will have gone through every possible sum it could have need to make and the correct time will be store in open/close_time. 

The time changes are done using the shift function from arrow. The corresponding hours and minutes that must be stacked on are calculated using modf function which separate the integer and decimal part of our calculation, the decimal part is then multiplied by 60 to get the mins.

open_time() special cases:

* If the distance of the control is larger than 20% of our total brevet distance we set the open time to an hour less from the start time, this is an indicator that is caught in calc.html so we know to show the ERROR
* If this is not the case but the control distance is larger than the brevet distance but not by more than 20% we just set km_left to the total brevet dist and calculate the time as if there is no extra km. For example if the control is at 205km in a 200km brevet the time for that control will just be calculated as if the control was at 200km.

close_time() special cases:

* In this function we also have another variable called limits  which holds the hour and minute limits for each brevet distance
* Close times for control distances shorter than 60km we calculate them with a different formula which is the control_dist/20 + 1 hour
* If the control is further than 20% of the total brevet distance time is set to 1 hour less of start time to be caught later (same as in open_time())
* If our control is larger or equal than the total brevet distance but not larger than 20% we set our close time to the limit time for each brevet distance


### test_acp_times.py:

This file implements a suite of 5 test cases using nose. Functions are carefully called with parameters in the same format as when they are called from the flask_brevets.py file and are compared to the same format they are output.

* test_normal200() - basic calculation of a control distance wihin the first 200km of a 200km brevet
* test_under60() - checks for correct implementation of the logic for close time with short distances
* test_longerdistance() - checks a distance that is longer than the brevet distance but not long enough that it would generate an error
* test_error() - checks that distances 20% longer than brevet distance are correctly caught
* test_concatenated_sums() - checks a case in which it goes through every length section and must make different calculations for each part of the brevet
